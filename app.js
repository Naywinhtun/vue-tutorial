// const inputbox = document.querySelector('#courseName');
// const button = document.querySelector('#add');
// const listElement = document.querySelector('#my-lists');

// function addCourse() {
//     const listItem = document.createElement('li');
//     listItem.textContent = inputbox.value;
//     listElement.append(listItem);
//     inputbox.value = ' ';
// }

// button.addEventListener('click', addCourse);

const app = new Vue({
  el: "#app",
  data() {
    return {
      normalCount: 0,
      counter: 1,
      coursename: "",
      name: "John Doe",
      lastname : '',
      courses: [],
    };
  },

  watch: {
    counter () {
        if( this.counter > 50 ){
            this.counter = 0;
            this.normalCount ++;
        }
    }
  },

  computed: {
    fullname() {
        if(this.lastname === ''){    
            return ''; 
        }
        return this.name + ' ' + this.lastname;
    }
  },

  methods: {
    setName(event) {
      this.coursename = event.target.value;
    },

    reset() {
      this.coursename = "";
    },

    addCourse() {
      this.courses.push(this.coursename);
      this.coursename = "";
    },

    removeCourse(index) {
      this.courses.splice(index, 1);
    },
    Add(num) {
      this.counter = this.counter + num;
    },

    Sub(num) {
      this.counter = this.counter - num;
    },
  },
});